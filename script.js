let theme = document.querySelector('#theme');
let btn = document.querySelector('.theme__btn');

btn.addEventListener('click', () => {
	if (theme.getAttribute("href") === "./css/light_style.css") {
		theme.href = './css/dark_style.css';
	 } else {
		theme.href = './css/light_style.css';
	 }
});